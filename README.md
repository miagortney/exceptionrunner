Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?

    Logger.log allows the developer to log messages at different levels, such as Severe, Warning, or Info. 
    This helps the developer to distinguish messages as they are testing their code. Logging to the console 
    simply prints the message given to the function straight to the console. There are no levels attached at all.

2.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

    This line comes from the logger.log file, which keeps track of what is happening in the Junit tests. This message 
    is due to the failing of the Test Timer fail test. The failed test throws the wrong kind of exception, a 
    NullPointerException instead of a TimerException, which results in this message.

3.  What does Assertions.assertThrows do?

    Assertions.assertThrows evaluates if a certain exception was thrown or not in a method. It evaluates to true or 
    false, which will then show in the Junit test as the test having failed or succeeded.

4.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
    
        serialVersionUID is used to remember different versions of a Serialized class to verify that a loaded class 
        and the serialized object are compatible. I believe this is used to ensure that different instances of 
        TimerException are compatible with the Timer class.
    
    2.  Why do we need to override constructors?
    
        We need to override the constructor because TimerException extends the Exception class. If the constructor 
        was not overridden, the TimerException constructor would call the Exception constructor, which would mess up 
        the messages being delivered. To keep the functionality within TimerException, the constructors being used 
        must be overridden.
    
    3.  Why we did not override other Exception methods?
    
        Other Exception methods in TimerException do not need to be overwritten because the Junit tests are set up so it 
        only calls the exceptions that are already overwritten. There is no need to overwrite the missing ones, because 
        they will never be used in this program.
    
5.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

    The static block in Timer.java is ran at the beginning of the program. It sets up the logger with its indicated properties 
    in the logger.properties file. The LogManager also reads the configuration from the configuration file and then closes it. 
    If this fails, a message will be print to the console, indicating that configuration of the logger failed.

6.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

    The README.md file is used in bitbucket to describe the contents of the repository. If it is included at the root of the 
    repository, its contents are displayed on the source page of the repository.

7.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)

    The test is failing because the assertThrows is catching the wrong exception. A NullPointerException is being thrown 
    instead of the expected exception which is TimerException. This other exception is being thrown because after the 
    TimerException is thrown, the system moves to the finally{} block where System.currentTimeMillis() – timeNow is 
    calculated. timeNow is null at this point, so the NullPointerException is thrown.
	This can be fixed by setting timeNow before potentially throwing the exception rather than after. This way timeNow 
    won’t have the potential to be null.

8.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)

    In the Test Timer fail Junit test, the sequence of Exceptions and handlers is as follows:

    1.	The Test Timer fail test runs the assertThrows condition with the code statement Timer.timeMe(-1).
    2.	In Timer.java, within the timeMe() function, the method tries to set timeNow, check that timeToWait is greater 
        or equal to 0, and call method with timeToWait. An InterruptedException is caught if it is thrown here.
    3.	If timeToWait is less than 0, then the TimerException is thrown. 
    4.	Since an exception is not thrown in the try section in timeMe(), the functionality is moved to the finally {} block. 
        In the finally block, an operation is carried out with timeNow. If timeNow is null, a NullPointerException is thrown, 
        making it the most recent exception called in the assertThrows() function.
    5.	This can be fixed by setting timeNow’s value before throwing the TimerException, ensuring that it is not null.

9.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 

    Included in pdf.

10. Make a printScreen of your eclipse Maven test run, with console

    Included in pdf.

11. What category of Exceptions is TimerException and what is NullPointerException?

    TimerException is a Throwable: Exception. It is allocated its own class, which an instance of will be created when 
    the exception is thrown. NullPointerException is a RuntimeException. The only time this exception is thrown is if 
    the programmer made a mistake and a variable is null when it isn’t supposed to be. This mistake will only be found 
    when the program runs.

12. Push the updated/fixed source code to your own repository.

    Link included in pdf.